package com.tatya.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.tatya.bean.entity.PropertyType;

@Repository
public interface PropertyTypeRepository extends JpaRepository<PropertyType, Integer>, JpaSpecificationExecutor<PropertyType> {

	@Query("SELECT pt FROM PropertyType pt WHERE pt.type=?1")
	public List<PropertyType> findByType(String type);
	
	@Query("SELECT pt FROM PropertyType pt WHERE pt.name=?1")
	public PropertyType findByName(String name);
	
}
