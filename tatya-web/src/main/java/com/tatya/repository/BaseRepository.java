package com.tatya.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.support.PageableExecutionUtils;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.Collections;
import java.util.List;
import java.util.function.LongSupplier;

@SuppressWarnings("deprecation")
@NoRepositoryBean
public abstract class BaseRepository {

	protected <T, S> Page<T> readPage(TypedQuery<T> query, Pageable pageable, Specification<S> spec, Class<S> domainClass) {
        query.setFirstResult(Long.valueOf(pageable.getOffset()).intValue());
        query.setMaxResults(pageable.getPageSize());

        return PageableExecutionUtils.getPage(query.getResultList(), pageable, new LongSupplier() {
            @Override
            public long getAsLong() {
                return executeCountQuery(getCountQuery(spec, domainClass));
            }
        });
    }

    protected Long executeCountQuery(TypedQuery<Long> query) {

        List<Long> totals = query.getResultList();
        Long total = 0L;

        for (Long element : totals) {
            total += element == null ? 0 : element;
        }

        return total;
    }

    protected <S> TypedQuery<Long> getCountQuery(Specification<S> spec, Class<S> domainClass) {

        CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);

        Root<S> root = applySpecificationToCriteria(spec, domainClass, query);

        if (query.isDistinct()) {
            query.select(builder.countDistinct(root));
        } else {
            query.select(builder.count(root));
        }

        query.orderBy(Collections.<Order>emptyList());

        return getEntityManager().createQuery(query);
    }

    protected <S, U> Root<U> applySpecificationToCriteria(Specification<U> spec, Class<U> domainClass,
                                                          CriteriaQuery<S> query) {

        Root<U> root = query.from(domainClass);

        if (spec == null) {
            return root;
        }

        CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        Predicate predicate = spec.toPredicate(root, query, builder);

        if (predicate != null) {
            query.where(predicate);
        }

        return root;
    }

    public abstract EntityManager getEntityManager();
}