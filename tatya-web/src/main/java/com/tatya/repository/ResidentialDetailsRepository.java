package com.tatya.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tatya.bean.entity.ResidentialDetails;

public interface ResidentialDetailsRepository extends JpaSpecificationExecutor<ResidentialDetails>, JpaRepository<ResidentialDetails, Integer> {

}
