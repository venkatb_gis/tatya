package com.tatya.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tatya.bean.entity.ResRentDetails;

public interface ResRentRepository extends JpaSpecificationExecutor<ResRentDetails>, JpaRepository<ResRentDetails, Integer> {

}
