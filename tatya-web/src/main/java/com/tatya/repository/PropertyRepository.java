package com.tatya.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tatya.bean.entity.Property;

public interface PropertyRepository extends JpaSpecificationExecutor<Property>, JpaRepository<Property, Integer> {

}
