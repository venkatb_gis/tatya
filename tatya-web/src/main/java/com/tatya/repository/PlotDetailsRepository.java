package com.tatya.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tatya.bean.entity.PlotDetails;

public interface PlotDetailsRepository extends JpaSpecificationExecutor<PlotDetails>, JpaRepository<PlotDetails, Integer> {

}
