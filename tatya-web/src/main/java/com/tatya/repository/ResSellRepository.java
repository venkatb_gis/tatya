package com.tatya.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tatya.bean.entity.ResSellDetails;

public interface ResSellRepository extends JpaSpecificationExecutor<ResSellDetails>, JpaRepository<ResSellDetails, Integer> {

}
