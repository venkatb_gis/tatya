package com.tatya.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tatya.bean.entity.PropertyMedia;

public interface PropertyMediaRepository extends JpaSpecificationExecutor<PropertyMedia>, JpaRepository<PropertyMedia, Integer> {

}
