package com.tatya.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.tatya.bean.entity.TenantType;

public interface TenantTypeRepository extends JpaSpecificationExecutor<TenantType>, JpaRepository<TenantType, Integer> {
	
	@Query("SELECT tt FROM TenantType tt WHERE tt.name=?1")
	public TenantType findByName(String name);

}
