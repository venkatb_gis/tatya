package com.tatya.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tatya.bean.dto.SellDTO;
import com.tatya.bean.entity.PGDetails;
import com.tatya.bean.entity.PlotDetails;
import com.tatya.bean.entity.Property;
import com.tatya.bean.entity.PropertyMedia;
import com.tatya.bean.entity.PropertyType;
import com.tatya.bean.entity.ResRentDetails;
import com.tatya.bean.entity.ResSellDetails;
import com.tatya.bean.entity.ResidentialDetails;
import com.tatya.bean.entity.TenantType;
import com.tatya.repository.PGDetailsRepository;
import com.tatya.repository.PlotDetailsRepository;
import com.tatya.repository.PropertyMediaRepository;
import com.tatya.repository.PropertyRepository;
import com.tatya.repository.PropertyTypeRepository;
import com.tatya.repository.ResRentRepository;
import com.tatya.repository.ResSellRepository;
import com.tatya.repository.ResidentialDetailsRepository;
import com.tatya.repository.TenantTypeRepository;

@Service
public class SellServiceImpl implements SellService {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(SellServiceImpl.class);

	@Autowired
	private PropertyTypeRepository propertyTypeRepository;

	@Autowired
	private PropertyRepository propertyRepository;

	@Autowired
	private PropertyMediaRepository propertyMediaRepository;

	@Autowired
	private ResidentialDetailsRepository residentialDetailsRepository;

	@Autowired
	private ResRentRepository resRentRepository;

	@Autowired
	private ResSellRepository resSellRepository;

	@Autowired
	private PlotDetailsRepository plotDetailsRepository;

	@Autowired
	private PGDetailsRepository pgDetailsRepository;

	@Autowired
	private TenantTypeRepository tenantTypeRepository;

	@Override
	public List<Property> findAllProperty() {
		List<Property> propertylist = propertyRepository.findAll();
//		log.info("propertylist: {}", propertylist);
		return propertylist;

	}

	@Override
	public Property save(SellDTO sellDTO) {

		Property property = new Property();
		property.setCity(sellDTO.getCity());
		property.setLocality(sellDTO.getLocality());
		property.setAddress(sellDTO.getAddress());
		property = propertyRepository.save(property);
		if (sellDTO.getPropertyType() != null)
			;
		PropertyType PropertyType = propertyTypeRepository.findByName(sellDTO.getPropertyType());

		if (sellDTO.getCategory().equalsIgnoreCase("RESIDENTIAL") && !sellDTO.getService().equalsIgnoreCase("PG")
				&& sellDTO.getPropertyType() != null && PropertyType.getType().equalsIgnoreCase("RESIDENTIAL")) {
			com.tatya.bean.dto.SellDTO.ResidentialDetails residentialDetailsDTO = sellDTO.getResidentialDetails();
			ResidentialDetails residentialDetails = new ResidentialDetails();
			residentialDetails.setPropertyType(PropertyType);
			residentialDetails.setPropertyAge(residentialDetailsDTO.getPropertyAge());
			residentialDetails.setBhk(residentialDetailsDTO.getBhk());
			residentialDetails.setBathroom(residentialDetailsDTO.getBathroom());
			residentialDetails.setBalcony(residentialDetailsDTO.getBalcony());
			residentialDetails.setFurnishType(residentialDetailsDTO.getFurnishType());
			residentialDetails.setCoverParking(residentialDetailsDTO.getCoverParking());
			residentialDetails.setOpenParking(residentialDetailsDTO.getOpenParking());
			residentialDetails.setMaintenanceCharges(residentialDetailsDTO.getMaintenanceCharges());
			residentialDetails.setBuiltArea(residentialDetailsDTO.getBuiltArea());
			residentialDetails.setCarpetArea(residentialDetailsDTO.getCarpetArea());
			residentialDetails.setOffers(residentialDetailsDTO.getOffers());
			residentialDetails = residentialDetailsRepository.save(residentialDetails);
			if (sellDTO.getService().equalsIgnoreCase("RENT")) {
				ResRentDetails rentDetails = new ResRentDetails();
				rentDetails.setAvailableDate(residentialDetailsDTO.getRentDetails().getAvailableFrom());
				rentDetails.setMonthlyRent(residentialDetailsDTO.getRentDetails().getMonthlyRent());
				rentDetails.setSecurityDeposit(residentialDetailsDTO.getRentDetails().getSecurityDeposit());
				rentDetails.setRentDetail(residentialDetails);
				ArrayList<TenantType> tenantTypes = new ArrayList<TenantType>();
				for (String tenantType : residentialDetailsDTO.getRentDetails().getTenantTypes()) {
					TenantType tenantTypeObject = tenantTypeRepository.findByName(tenantType);
					tenantTypes.add(tenantTypeObject);
				}
				rentDetails = resRentRepository.save(rentDetails);
				rentDetails.setTenantTypes(tenantTypes);
				residentialDetails.setRentDetails(rentDetails);
			}
			if (sellDTO.getService().equalsIgnoreCase("SELL")) {
				ResSellDetails sellDetails = new ResSellDetails();
				sellDetails.setPossessionDate(residentialDetailsDTO.getSellDetails().getPossessionDate());
				sellDetails.setCost(residentialDetailsDTO.getSellDetails().getCost());
				sellDetails.setConstructionStatus(residentialDetailsDTO.getSellDetails().getConstructionStatus());
				sellDetails.setSellDetail(residentialDetails);
				sellDetails = resSellRepository.save(sellDetails);
				residentialDetails.setSellDetails(sellDetails);
			}
			residentialDetails.setResProperty(property);
			residentialDetails = residentialDetailsRepository.save(residentialDetails);

			property.setResidentialDetails(residentialDetails);
		}

		if (sellDTO.getCategory().equalsIgnoreCase("RESIDENTIAL") && !sellDTO.getService().equalsIgnoreCase("PG")
				&& sellDTO.getPropertyType() != null && PropertyType.getType().equalsIgnoreCase("PLOT")) {
			com.tatya.bean.dto.SellDTO.PlotDetails plotDetailsDTO = sellDTO.getPlotDetails();
			PlotDetails plotDetails = new PlotDetails();
			plotDetails.setPossessionStatus(plotDetailsDTO.getPossessionStatus());
			plotDetails.setAvailableDate(plotDetailsDTO.getAvailableDate());
			plotDetails.setCost(plotDetailsDTO.getCost());
			plotDetails.setMaintenanceCharges(plotDetailsDTO.getMaintenanceCharges());
			plotDetails.setPlotArea(plotDetailsDTO.getPlotArea());
			plotDetails.setLength(plotDetailsDTO.getLength());
			plotDetails.setWidth(plotDetailsDTO.getWidth());
			plotDetails.setRoadFacingWidth(plotDetailsDTO.getRoadFacingWidth());
			plotDetails.setOffers(plotDetailsDTO.getOffers());
			plotDetails.setPlotProperty(property);
			plotDetails = plotDetailsRepository.save(plotDetails);
			property.setPlotDetails(plotDetails);
		}

		if (sellDTO.getCategory().equalsIgnoreCase("RESIDENTIAL") && sellDTO.getService().equalsIgnoreCase("PG")
				&& sellDTO.getPropertyType() == null) {
			com.tatya.bean.dto.SellDTO.PGDetails pgDetailDTO = sellDTO.getPgDetails();
			PGDetails pgDetails = new PGDetails();
			pgDetails.setName(pgDetailDTO.getName());
			pgDetails.setTotalBeds(pgDetailDTO.getTotalBeds());
			pgDetails.setAvailable_beds(pgDetailDTO.getAvailableBeds());
			pgDetails.setMale(pgDetailDTO.getMale());
			pgDetails.setFemale(pgDetailDTO.getFemale());
			pgDetails.setStudents(pgDetailDTO.getStudents());
			pgDetails.setProfessionals(pgDetailDTO.getProfessionals());
			pgDetails.setMeals(pgDetailDTO.getMeals());
			pgDetails.setNoticePeriodDays(pgDetailDTO.getNoticePeriodDays());
			pgDetails.setLockInPeriodDatys(pgDetailDTO.getLockInPeriodDatys());
			pgDetails.setManagedBy(pgDetailDTO.getManagedBy());
			pgDetails.setManagerStayingAtProperty(pgDetailDTO.getManagerStayingAtProperty());
			pgDetails.setNonVeg(pgDetailDTO.getNonVeg());
			pgDetails.setOppSex(pgDetailDTO.getOppSex());
			pgDetails.setAnyTime(pgDetailDTO.getAnyTime());
			pgDetails.setVisitors(pgDetailDTO.getVisitors());
			pgDetails.setGuardian(pgDetailDTO.getGuardian());
			pgDetails.setDrinking(pgDetailDTO.getDrinking());
			pgDetails.setSmoking(pgDetailDTO.getSmoking());
			pgDetails.setPgProperty(property);
			pgDetails = pgDetailsRepository.save(pgDetails);
			property.setPGDetails(pgDetails);
		}
		if (sellDTO.getPropertyMedias() != null && sellDTO.getPropertyMedias().length > 0) {
			ArrayList<PropertyMedia> propertymediaList = new ArrayList<PropertyMedia>();
			for (com.tatya.bean.dto.SellDTO.PropertyMedia pmDTO : sellDTO.getPropertyMedias()) {
				PropertyMedia propertyMedia = new PropertyMedia();
				propertyMedia.setMediaUrl(pmDTO.getMediaUrl());
				propertyMedia.setMediaType(pmDTO.getMediaType());
				propertyMedia.setMediaProperty(property);
				propertyMedia = propertyMediaRepository.save(propertyMedia);
				propertymediaList.add(propertyMedia);
			}
			property.setPropertyMedias(propertymediaList);
		}
		propertyRepository.save(property);
		return property;

	}

	@Override
	public List<PropertyType> findAll() {
		List<PropertyType> propertylist = propertyTypeRepository.findAll();
		return propertylist;

	}

}
