package com.tatya.service;

import java.util.List;

import com.tatya.bean.dto.SellDTO;
import com.tatya.bean.entity.Property;
import com.tatya.bean.entity.PropertyType;

public interface SellService {
	
	public List<Property> findAllProperty();

	public Property save(SellDTO sell);
	
	public List<PropertyType> findAll();
}
