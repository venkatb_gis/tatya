package com.tatya.bean.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties({"id", "resProperty"})
@Entity
@Table(name = "tp_residential_details")
@Data
public class ResidentialDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "property_type_id")
	private PropertyType propertyType;

	@Column(name = "property_age")
	private Integer propertyAge;

	@Column(name = "no_of_bhk")
	private Integer bhk;

	@Column(name = "no_of_batroom")
	private Integer bathroom;

	@Column(name = "no_of_balcony")
	private Integer balcony;

	@Column(name = "furnish_type")
	private String furnishType;

	@Column(name = "no_cover_parking")
	private Integer coverParking;

	@Column(name = "no_of_open_parking")
	private Integer openParking;

	@Column(name = "maintenance_charges")
	private Integer maintenanceCharges;

	@Column(name = "built_area")
	private Integer builtArea;

	@Column(name = "carpet_area")
	private Integer carpetArea;

	@Column(name = "offers")
	private String offers;

    @OneToOne(mappedBy = "rentDetail")
    private ResRentDetails rentDetails;
    
    @OneToOne(mappedBy = "sellDetail")
    private ResSellDetails sellDetails;

    @OneToOne
    @JoinColumn(name = "property_id")
    private Property resProperty;

}
