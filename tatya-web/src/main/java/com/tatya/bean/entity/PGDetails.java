package com.tatya.bean.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties({"id", "pgProperty"})
@Entity
@Table(name = "tp_pg_details")
@Data
public class PGDetails {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
	
	@OneToOne
	@JoinColumn(name = "property_id")
	private Property pgProperty;
	
	@Column(name = "name")
    private String name;
	
	@Column(name = "total_beds")
    private Integer totalBeds;
	
	@Column(name = "available_beds")
    private Integer available_beds;
	
	@Column(name = "male")
    private Boolean male;
	
	@Column(name = "female")
    private Boolean female;
	
	@Column(name = "students")
    private Boolean students;
	
	@Column(name = "professionals")
    private Boolean professionals;
	
	@Column(name = "meals")
    private Boolean meals;
	
	@Column(name = "notice_period_days")
    private Integer noticePeriodDays;
	
	@Column(name = "lock_in_period_datys")
    private Integer lockInPeriodDatys;
	
	@Column(name = "managed_by")
    private String managedBy;
	
	@Column(name = "manager_staying_at_property")
    private Boolean managerStayingAtProperty;
	
	@Column(name = "non_veg")
    private Boolean nonVeg;
	
	@Column(name = "opp_sex")
    private Boolean oppSex;
	
	@Column(name = "any_time")
    private Boolean anyTime;
	
	@Column(name = "visitors")
    private Boolean visitors;
	
	@Column(name = "guardian")
    private Boolean guardian;
	
	@Column(name = "drinking")
    private Boolean drinking;
	
	@Column(name = "smoking")
    private Boolean smoking;

}
