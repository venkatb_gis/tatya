package com.tatya.bean.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties({"id", "sellDetail"})
@Entity
@Table(name = "tp_res_sell_details")
@Data
public class ResSellDetails {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
	
	@Column(name = "construction_status")
    private String constructionStatus;
	
	@Column(name = "cost")
    private Integer cost;
	
	@Column(name = "possession_date")
    private Date possessionDate;
	
	@OneToOne
    @JoinColumn(name = "resdential_details_id")
    private ResidentialDetails sellDetail;
	

}
