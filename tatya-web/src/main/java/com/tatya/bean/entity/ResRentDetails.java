package com.tatya.bean.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties({"id", "rentDetail"})
@Entity
@Table(name = "tp_res_rent_details")
@Data
public class ResRentDetails {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
	
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name = "available_date")
    private Date availableDate;
	
	@Column(name = "monthly_rent")
    private Integer monthlyRent;
	
	@Column(name = "security_deposit")
    private Integer securityDeposit;
	
	@OneToOne
    @JoinColumn(name = "residential_details_id")
    private ResidentialDetails rentDetail;
	
	@ManyToMany
    @JoinTable(name = "tp_rent_tenant_type", joinColumns = {@JoinColumn(name = "res_rent_id")}, inverseJoinColumns = {@JoinColumn(name = "tenant_type_id")})
    private List<TenantType> tenantTypes;
	

}
