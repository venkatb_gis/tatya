package com.tatya.bean.entity;

import java.util.List;

import javax.persistence.*;

import lombok.Data;

@Entity
@Table(name = "tp_property")
@Data
public class Property {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    
    @OneToOne(mappedBy = "resProperty")
    private ResidentialDetails residentialDetails;
    
//    @OneToOne
//	@JoinColumn(name = "commercial_id")
//    private CommercialDetails commercialDetails;
    
    @OneToOne(mappedBy = "pgProperty")
    private PGDetails PGDetails;
    
    @OneToMany(mappedBy = "mediaProperty")
    private List<PropertyMedia> propertyMedias;
    
    
    @OneToOne(mappedBy = "plotProperty")
    private PlotDetails plotDetails;
    
    @Column(name = "city")
    private String city;
    
    @Column(name = "locality")
    private String locality;
    
    @Column(name = "address")
    private String address;
    
}
