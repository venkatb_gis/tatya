package com.tatya.bean.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties({"id", "mediaProperty"})
@Entity
@Table(name = "tp_property_media")
@Data
public class PropertyMedia {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "media_url")
	private String mediaUrl;

	@Column(name = "media_type")
	private String mediaType;

	@ManyToOne
	@JoinColumn(name = "property_id")
	private Property mediaProperty;
}
