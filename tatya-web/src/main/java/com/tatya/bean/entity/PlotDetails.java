package com.tatya.bean.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties({"id", "plotProperty"})
@Entity
@Table(name = "tp_plot_details")
@Data
public class PlotDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@OneToOne
	@JoinColumn(name = "property_id")
	private Property plotProperty;

	@ManyToOne
	@JoinColumn(name = "property_type_id")
	private PropertyType propertyType;

	@Column(name = "possessionStatus")
	private String possessionStatus;

	@Column(name = "available_date")
	private Date availableDate;

	@Column(name = "cost")
	private Integer cost;

	@Column(name = "maintenance_charges")
	private Integer maintenanceCharges;

	@Column(name = "plot_area")
	private Integer plotArea;

	@Column(name = "length")
	private Integer length;

	@Column(name = "width")
	private Integer width;

	@Column(name = "road_facing_width")
	private Integer roadFacingWidth;

	@Column(name = "offers")
	private String offers;

}
