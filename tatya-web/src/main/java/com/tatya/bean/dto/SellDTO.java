package com.tatya.bean.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties
@Data
public class SellDTO {

	private String category;
	private String service;
	private String propertyType;
	private ResidentialDetails residentialDetails;
	private PlotDetails plotDetails;
	private PGDetails pgDetails;
	private PropertyMedia[] propertyMedias;
	private String city;
	private String locality;
	private String address;

	@JsonIgnoreProperties
	@Data
	public static class ResidentialDetails {
		private Integer propertyAge;
		private Integer bhk;
		private Integer bathroom;
		private Integer balcony;
		private String furnishType;
		private Integer coverParking;
		private Integer openParking;
		private Integer maintenanceCharges;
		private Integer builtArea;
		private Integer carpetArea;
		private String offers;
		private RentDetails rentDetails;
		private SellDetails sellDetails;
		
		@JsonIgnoreProperties
		@Data
		public static class RentDetails {
			private Date availableFrom;
			private Integer monthlyRent;
			private Integer securityDeposit;
			private String[] tenantTypes;
		};

		@JsonIgnoreProperties
		@Data
		public static class SellDetails {
			private String constructionStatus;
			private Integer cost;
			private Date possessionDate;
		}
	}

	@JsonIgnoreProperties
	@Data
	public static class PlotDetails {
		private String possessionStatus;
		private Date availableDate;
		private Integer cost;
		private Integer maintenanceCharges;
		private Integer plotArea;
		private Integer length;
		private Integer width;
		private Integer roadFacingWidth;
		private String offers;
	}

	@JsonIgnoreProperties
	@Data
	public static class PGDetails {
		private String name;
		private Integer totalBeds;
		private Integer availableBeds;
		private Boolean male;
		private Boolean female;
		private Boolean students;
		private Boolean professionals;
		private Boolean meals;
		private Integer noticePeriodDays;
		private Integer lockInPeriodDatys;
		private String managedBy;
		private Boolean managerStayingAtProperty;
		private Boolean nonVeg;
		private Boolean oppSex;
		private Boolean anyTime;
		private Boolean visitors;
		private Boolean guardian;
		private Boolean drinking;
		private Boolean smoking;
	}
	
	@JsonIgnoreProperties
	@Data
	public static class PropertyMedia {
		private String mediaUrl;
		private String mediaType;
	}

}
