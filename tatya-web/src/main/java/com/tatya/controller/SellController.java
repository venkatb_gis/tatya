package com.tatya.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tatya.bean.dto.SellDTO;
import com.tatya.bean.entity.Property;
import com.tatya.bean.entity.PropertyType;
import com.tatya.service.SellService;

@RestController
@RequestMapping("/api")
public class SellController {

	@Autowired
	private SellService sellService;
	
	@RequestMapping(value = "/property", method = RequestMethod.GET)
	public List<Property> getProperty() {
		return sellService.findAllProperty();
	}
	
	@RequestMapping(value = "/property", method = RequestMethod.POST)
	public Property postProperty(@Validated @RequestBody SellDTO sellDTO) {
		return sellService.save(sellDTO);
	}
	
	@RequestMapping(value = "/propertytype", method = RequestMethod.GET)
	public List<PropertyType> GetPropertyTypes() {
		return sellService.findAll();
	}
	
}
