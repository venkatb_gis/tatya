-- MySQL 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: tatya
-- ------------------------------------------------------
-- Server version	8.0.26

--
-- Table structure for table `tp_property`
--

DROP TABLE IF EXISTS `tp_property`;
CREATE TABLE `tp_property` (
  `id` int NOT NULL AUTO_INCREMENT,
  `city` varchar(45) DEFAULT NULL,
  `locality` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `created_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Table structure for table `tp_pg_details`
--

DROP TABLE IF EXISTS `tp_pg_details`;
CREATE TABLE `tp_pg_details` (
  `id` int NOT NULL AUTO_INCREMENT,
  `property_id` int DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `total_beds` int DEFAULT NULL,
  `available_beds` int DEFAULT NULL,
  `male` char(1) DEFAULT '0',
  `female` char(1) DEFAULT '0',
  `students` char(1) DEFAULT '0',
  `professionals` char(1) DEFAULT '0',
  `meals` char(1) DEFAULT '0',
  `notice_period_days` int DEFAULT NULL,
  `lock_in_period_datys` int DEFAULT NULL,
  `managed_by` varchar(45) DEFAULT NULL,
  `manager_staying_at_property` char(1) DEFAULT '0',
  `non_veg` char(1) DEFAULT '0',
  `opp_sex` char(1) DEFAULT '0',
  `any_time` char(1) DEFAULT '0',
  `visitors` char(1) DEFAULT '0',
  `guardian` char(1) DEFAULT '0',
  `drinking` char(1) DEFAULT '0',
  `smoking` char(1) DEFAULT '0',
  `created_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_tp_pg_details_property_id_idx` (`property_id`),
  CONSTRAINT `fk_tp_pg_details_property_id` FOREIGN KEY (`property_id`) REFERENCES `tp_property` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Table structure for table `tp_plot_details`
--

DROP TABLE IF EXISTS `tp_plot_details`;
CREATE TABLE `tp_plot_details` (
  `id` int NOT NULL AUTO_INCREMENT,
  `property_id` int DEFAULT NULL,
  `property_type_id` int DEFAULT NULL,
  `possession_status` varchar(45) DEFAULT NULL,
  `available_date` timestamp NULL DEFAULT NULL,
  `cost` int DEFAULT NULL,
  `maintenance_charges` int DEFAULT NULL,
  `tp_plot_detailscol` varchar(45) DEFAULT NULL,
  `plot_area` int DEFAULT NULL,
  `length` int DEFAULT NULL,
  `width` int DEFAULT NULL,
  `road_facing_width` int DEFAULT NULL,
  `offers` varchar(45) DEFAULT NULL,
  `created_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_tp_plot_details_property_id_idx` (`property_id`),
  CONSTRAINT `fk_tp_plot_details_property_id` FOREIGN KEY (`property_id`) REFERENCES `tp_property` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Table structure for table `tp_property_media`
--

DROP TABLE IF EXISTS `tp_property_media`;
CREATE TABLE `tp_property_media` (
  `id` int NOT NULL AUTO_INCREMENT,
  `property_id` int NOT NULL,
  `media_url` varchar(45) DEFAULT NULL,
  `media_type` varchar(45) NOT NULL,
  `created_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_tp_property_media_pty_idx` (`property_id`),
  CONSTRAINT `fk_tp_property_media_pty` FOREIGN KEY (`property_id`) REFERENCES `tp_property` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Table structure for table `tp_residential_details`
--

DROP TABLE IF EXISTS `tp_residential_details`;
CREATE TABLE `tp_residential_details` (
  `id` int NOT NULL AUTO_INCREMENT,
  `property_type_id` int DEFAULT NULL,
  `property_age` int DEFAULT NULL,
  `no_of_bhk` int DEFAULT NULL,
  `no_of_batroom` int DEFAULT NULL,
  `no_of_balcony` int DEFAULT NULL,
  `furnish_type` varchar(45) DEFAULT NULL,
  `no_cover_parking` int DEFAULT NULL,
  `no_of_open_parking` int DEFAULT NULL,
  `maintenance_charges` int DEFAULT NULL,
  `built_area` int DEFAULT NULL,
  `carpet_area` int DEFAULT NULL,
  `offers` varchar(150) DEFAULT NULL,
  `property_id` int DEFAULT NULL,
  `created_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_tp_residential_details_property_id_idx` (`property_id`),
  CONSTRAINT `fk_tp_residential_details_property_id` FOREIGN KEY (`property_id`) REFERENCES `tp_property` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Table structure for table `tp_res_rent_details`
--

DROP TABLE IF EXISTS `tp_res_rent_details`;
CREATE TABLE `tp_res_rent_details` (
  `id` int NOT NULL AUTO_INCREMENT,
  `available_date` timestamp NULL DEFAULT NULL,
  `monthly_rent` int DEFAULT NULL,
  `security_deposit` int DEFAULT NULL,
  `residential_details_id` int DEFAULT NULL,
  `created_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_tp_res_rent_details_property_idx` (`residential_details_id`),
  CONSTRAINT `fk_tp_res_rent_details_property` FOREIGN KEY (`residential_details_id`) REFERENCES `tp_residential_details` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Table structure for table `tp_res_sell_details`
--

DROP TABLE IF EXISTS `tp_res_sell_details`;
CREATE TABLE `tp_res_sell_details` (
  `id` int NOT NULL AUTO_INCREMENT,
  `construction_status` varchar(45) DEFAULT NULL,
  `cost` int DEFAULT NULL,
  `possession_date` timestamp NULL DEFAULT NULL,
  `resdential_details_id` int DEFAULT NULL,
  `created_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_tp_res_sell_details_resdential_details_idx` (`resdential_details_id`),
  CONSTRAINT `fk_tp_res_sell_details_resdential_details` FOREIGN KEY (`resdential_details_id`) REFERENCES `tp_residential_details` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Table structure for table `tp_tenant_type`
--

DROP TABLE IF EXISTS `tp_tenant_type`;
CREATE TABLE `tp_tenant_type` (
  `id` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `value` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Table structure for table `tp_property_type`
--

DROP TABLE IF EXISTS `tp_property_type`;
CREATE TABLE `tp_property_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `type` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Table structure for table `tp_rent_tenant_type`
--

DROP TABLE IF EXISTS `tp_rent_tenant_type`;
CREATE TABLE `tp_rent_tenant_type` (
  `res_rent_id` int NOT NULL,
  `tenant_type_id` int NOT NULL,
  PRIMARY KEY (`res_rent_id`,`tenant_type_id`),
  KEY `fk_tp_rent_tenant_type_rent_idx` (`res_rent_id`),
  KEY `fk_tp_rent_tenant_type_tenant_type_idx` (`tenant_type_id`),
  CONSTRAINT `fk_tp_rent_tenant_type_rent` FOREIGN KEY (`res_rent_id`) REFERENCES `tp_res_rent_details` (`id`),
  CONSTRAINT `fk_tp_rent_tenant_type_tenant_type` FOREIGN KEY (`tenant_type_id`) REFERENCES `tp_tenant_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
