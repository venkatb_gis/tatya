-- MySQL Insert 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: tatya
-- ------------------------------------------------------
-- Server version	8.0.26

--
-- Insert data for table `tp_property_type`
--

LOCK TABLES `tp_property_type` WRITE;
/*!40000 ALTER TABLE `tp_property_type` DISABLE KEYS */;
INSERT INTO `tp_property_type` VALUES (1,'Apartment','RESIDENTIAL'),(2,'Independent Floor','RESIDENTIAL'),(3,'Independent House','RESIDENTIAL'),(4,'Villa','RESIDENTIAL'),(5,'Plot','PLOT'),(6,'Agricultural Land','PLOT'),(7,'Office','COMMERCIAL'),(8,'Shop','COMMERCIAL'),(9,'Showroom','COMMERCIAL');
/*!40000 ALTER TABLE `tp_property_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Insert data for table `tp_tenant_type`
--

LOCK TABLES `tp_tenant_type` WRITE;
/*!40000 ALTER TABLE `tp_tenant_type` DISABLE KEYS */;
INSERT INTO `tp_tenant_type` VALUES (1,'Family','FAMILY'),(2,'Bachelors','BACHELORS'),(3,'Company','COMPANY');
/*!40000 ALTER TABLE `tp_tenant_type` ENABLE KEYS */;
UNLOCK TABLES;

-- Insert completed 
